import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { enableProdMode } from '@angular/core';
import { AppModule } from "./app/app.module";
import { configureOAuthProviders } from "./app/services/auth-providers-helpers";
import * as firebase from "nativescript-plugin-firebase";

import { android } from "tns-core-modules/application";


if (android) {
}

configureOAuthProviders();

enableProdMode();
platformNativeScriptDynamic({ createFrameOnBootstrap: true }).bootstrapModule(
    AppModule
);