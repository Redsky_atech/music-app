import { Component, OnInit, AfterViewInit, OnChanges, OnDestroy } from "@angular/core";
import { ActivatedRoute } from "@angular/router"
import { RouterExtensions } from "nativescript-angular/router";
import { Constants } from "../../models/constants";
import { SelectedIndexChangedEventData } from "tns-core-modules/ui/tab-view";
import { Values } from "~/app/values/values";
import { Observable, EventData } from "tns-core-modules/data/observable";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { RadListView } from "nativescript-ui-listview";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Song } from "~/app/models/song";
import { Folder } from "tns-core-modules/file-system/file-system";
import { Page } from "tns-core-modules/ui/page/page";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { ImageSource, fromResource, fromBase64 } from "tns-core-modules/image-source";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
    page: Page
    isBusy: boolean = false;

    path: string;
    name: string = "Login";
    size: number;
    data = [];
    source: Observable;
    songs = new ObservableArray();
    mostViewedSongs = new ObservableArray();

    viewModel;
    user;
    pullRefreshLatest;
    pullRefreshMostViewed;

    imagePlayer: string;
    imagePlayerFocussed: string;
    list: RadListView
    rows;
    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;

    loggedIn: boolean = false;

    refstatus: boolean = false;

    public constant = new Constants();
    isRendering: boolean;
    isRenderingMostViewed: boolean;
    renderViewTimeout;

    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {
        this.tabSelectedIndex = 0;
        this.data = [];
        this.userService.actionBarSearch(true);

        this.userService.homeSelector(true)
        this.userService.actionBarState(true)
        this.userService.actionBarText('DJ RICK GEEZ')
        console.log("ConsBlock", ':Home')
        this.isRendering = false;
        this.isRenderingMostViewed = false;
        this.activatedRoute.queryParams.subscribe(params => {
            this.user = params["user"]
            if (this.user != null) {
                this.loggedIn = true;
                console.log("QeryBlock", ':Home')

            }
            else {
                if (Values.readString(Values.X_ROLE_KEY, "") != "") {
                    this.loggedIn = true;
                    console.log("QeryElseBlock", ':Home')

                }
                else {
                    console.log("QeryElsefalseBlock", ':Home')

                    this.loggedIn = false;
                }
            }
        })

        this.userService.userChanges.subscribe(user => {
            if (user == null || user == undefined) {
                this.loggedIn = false;
            }
            else {
                this.loggedIn = true;
            }
        })

        this.userService.homeUpdation.subscribe(data => {
            if (data == null || data == undefined) {
                this.loggedIn = false;
            }
            else {
                this.loggedIn = true;
            }
        })

        this.userService.drawerSwipe(true);
        this.imagePlayer = 'res://icon_video_play';
        this.imagePlayerFocussed = 'res://icon_video_play_hover';
    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
        this.getAllSongs(Values.readString(Values.X_ROLE_KEY, ""));
        this.getMostViewedSongs(Values.readString(Values.X_ROLE_KEY, ""));
        console.log("Page Loaded called")
    }

    pageUnloaded() {
        this.songs = new ObservableArray();
    }

    ngOnInit(): void {
    }

    ngAfterViewInit(): void {
    }

    ngOnChanges(): void {
        console.log("changes");
    }


    onSelectedIndexChanged(args: SelectedIndexChangedEventData) {
        if (args.oldIndex !== -1) {
            const newIndex = args.newIndex;
            if (newIndex === 0) {
                this.tabSelectedIndexResult = "Profile Tab (tabSelectedIndex = 0 )";
                this.tabSelectedIndex = 0;
            } else if (newIndex === 1) {
                this.tabSelectedIndexResult = "Stats Tab (tabSelectedIndex = 1 )";
            } else if (newIndex === 2) {
                this.tabSelectedIndexResult = "Settings Tab (tabSelectedIndex = 2 )";
            }
        }
    }
    refreshMostViewedTab(args) {
        this.pullRefreshMostViewed = args.object;

        this.pullRefreshMostViewed.refreshing = true;
        this.getMostViewedSongs(Values.readString(Values.X_ROLE_KEY, ""));
    }


    refreshLatestTab(args) {
        this.pullRefreshLatest = args.object;
        this.pullRefreshLatest.refreshing = true;

        this.getAllSongs(Values.readString(Values.X_ROLE_KEY, ""));
    }

    getAllSongs(xRoleKey: string) {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });
        this.isRendering = false;

        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {

                this.songs = new ObservableArray();
                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.songs.push(new Song(<Song>res.items[i]))
                    }
                }

                this.viewModel = new Observable();
                this.viewModel.set("items", this.songs);

                this.page.bindingContext = this.viewModel;

                let result: Folder[];
                result = <Folder[]>res.items;
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                this.pullRefreshLatest.refreshing = false;
            }

            else {
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                console.log(res.error)
                this.pullRefreshLatest.refreshing = false;
                return null;
            }
        },
            error => {
                this.renderViewTimeout = setTimeout(() => {
                    this.isRendering = true;
                }, 500)
                console.log(error)
                this.pullRefreshLatest.refreshing = false;
                return null;
            })
    }

    getThumbnailSrc(data: string) {
        if (data != null && data != undefined) {
            let base64Data = data.split(',');
            if (base64Data.length == 2) {
                if (base64Data[1] != null && base64Data[1] != undefined) {
                    const imageSrc = fromBase64(base64Data[1]);
                    return imageSrc;
                }
                else {
                    const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                    return imgFromResources;
                }
            } else {
                const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
                return imgFromResources;
            }

        }
        else {
            const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
            return imgFromResources;
        }
    }




    getMostViewedSongs(xRoleKey: string) {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });
        this.isRenderingMostViewed = false;

        this.http.get("http://docs-api-dev.m-sas.com/api/123/123/files?isMostViewed=true&&Page=1", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {

                if (res.items != undefined && res.items != null) {
                    for (var i = 0; i < res.items.length; i++) {
                        if (res.items[i].mimeType == "audio/mp3")
                            this.mostViewedSongs.push(new Song(<Song>res.items[i]))

                    }
                }

                this.renderViewTimeout = setTimeout(() => {
                    this.isRenderingMostViewed = true;
                }, 500)
                this.pullRefreshMostViewed.refreshing = false;

            }
            else {
                this.renderViewTimeout = setTimeout(() => {
                    this.isRenderingMostViewed = true;
                }, 500)
                // alert(res.error)
                this.pullRefreshMostViewed.refreshing = false;
                // return null;
            }
        },
            error => {
                this.renderViewTimeout = setTimeout(() => {
                    this.isRenderingMostViewed = true;
                }, 500)
                // alert(error)
                this.pullRefreshMostViewed.refreshing = false;
                // return null;
            })
    }

    onDrawerButtonTap(): void {
        this.userService.openDrawer();
    }

    cardClicked(song: Song) {

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "id": song.id,
                "name": song.name,
                "thumbnail": song.thumbnail,
                "url": song.url,
                "isFavourite": song.isFavourite,
                "views": song.views
            },
        };
        this.routerExtensions.navigate(["/detail"], extendedNavigationExtras)
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);

    }

}



