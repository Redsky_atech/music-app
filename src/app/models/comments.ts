import { CommentBy } from "./createdBy";
import { User } from "./user";

export class Comments {

    text: string;
    date: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.text = obj.text;
        this.date = obj.date;
    }
}