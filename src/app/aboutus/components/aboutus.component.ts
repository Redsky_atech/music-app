import { Component, OnInit, ElementRef, OnDestroy, ViewChild } from "@angular/core";
import { UserService } from "~/app/services/user.service";
import { DrawerTransitionBase, SlideInOnTopTransition, RadSideDrawer } from "nativescript-ui-sidedrawer";

@Component({
    selector: "AboutUs",
    moduleId: module.id,
    templateUrl: "./aboutus.component.html",
    styleUrls: ['./aboutus.component.css']
})

export class AboutUsComponent implements OnInit, OnDestroy {
    @ViewChild('rad') radDrawer: ElementRef;

    private _rad: RadSideDrawer;
    private _sideDrawerTransition: DrawerTransitionBase;
    constructor(private userService: UserService) {

        this.userService.actionBarState(true);
        this.userService.actionBarText('ABOUT THE APP');
        this.userService.actionBarSearch(false);
    }

    ngOnInit(): void {
        this._rad = this.radDrawer.nativeElement as RadSideDrawer;
        this.userService._radRef = this._rad;
        this._sideDrawerTransition = new SlideInOnTopTransition();
    }

    ngOnDestroy(): void {
    }


    getsideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    onDrawerButtonTap() {
        this._rad.showDrawer();
    }
}



