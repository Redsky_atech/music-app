import { GlobalNotificationBuilder } from './global-notification-instance'
import { getResources } from 'tns-core-modules/application/application';

@JavaProxy("cmom.tns.NotificationActionHandler")


class NotificationActionHandler extends android.content.BroadcastReceiver {

    ACTION_SNOOZE: string = "Snooze";
    globalNotificationBuilder: android.support.v4.app.NotificationCompat.Builder = null;

    constructor() {
        super();
    }

    onReceive(context: android.content.Context, intent: android.content.Intent) {
        console.log('Reachecd base')

        if (intent !== null) {
            if (intent.getAction() == 'Play') {
                this.globalNotificationBuilder = GlobalNotificationBuilder.getNotificationCompatBuilderInstance();
                var manager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
                var builder = new android.support.v4.app.NotificationCompat.Builder(context, 'NOTIFICATION');
                var actionIntent = GlobalNotificationBuilder.getActionIntent();

                actionIntent.setAction("Pause");
                actionIntent.putExtra('NOTIFICATION-ID', 'NOTIFICATION')
                GlobalNotificationBuilder.getSongService().play();

                var actionPendingIntent = android.app.PendingIntent.getBroadcast(context, 0, actionIntent, 0);
                builder.setContentTitle('Check out this new mix')
                    .setAutoCancel(true)
                    .setContentText("Song Title")
                    .setVibrate([100, 200, 100]) // optional
                    .setSmallIcon(context.getResources().getIdentifier("img_logo_splash", 
                    "drawable", context.getPackageName()))
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setContentIntent(GlobalNotificationBuilder.getMainPendingIntent())
                    .addAction(0, "Pause", actionPendingIntent)

                GlobalNotificationBuilder.setNotificationActionIntent(actionIntent);

                manager.notify(888, builder.build());
            }
            else if (intent.getAction() == 'Pause') {
                this.globalNotificationBuilder = GlobalNotificationBuilder.getNotificationCompatBuilderInstance();
                var manager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
                var builder = new android.support.v4.app.NotificationCompat.Builder(context, 'NOTIFICATION');

                var actionIntent = GlobalNotificationBuilder.getActionIntent();

                actionIntent.setAction("Play");
                actionIntent.putExtra('NOTIFICATION-ID', 'NOTIFICATION')
                GlobalNotificationBuilder.getSongService().pause();

                var actionPendingIntent = android.app.PendingIntent.getBroadcast(context, 0, actionIntent, 0);

                builder.setContentTitle('Check out this new mix')
                    .setAutoCancel(true)
                    .setContentText("Song Title")
                    .setVibrate([1000, 100, 1000, 100]) // optional
                    .setSmallIcon(context.getResources().getIdentifier("img_logo_splash", 
                    "drawable", context.getPackageName()))
                    .setOnlyAlertOnce(true)
                    .setOngoing(false)
                    .setContentIntent(GlobalNotificationBuilder.getMainPendingIntent())
                    .addAction(0, "Play", actionPendingIntent)

                GlobalNotificationBuilder.setNotificationActionIntent(actionIntent);

                manager.notify(888, builder.build());
            }
        }
    }
}
