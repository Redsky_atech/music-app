import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Values } from '../values/values';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';
import { AuthService } from './auth.service';
import * as connectivity from "connectivity";

@Injectable()
export class UserService {


    public isConnected: boolean;

    private _networkSubject = new Subject<boolean>();
    private _userSubject = new Subject<any>();
    private _homeSubject = new Subject<boolean>();
    private _homeUpdationSubject = new Subject<any>();
    private _actionBarState = new Subject<boolean>();
    private _actionBarText = new Subject<string>();
    private _playerButton = new Subject<boolean>();
    private _actionBarSearch = new Subject<boolean>();
    private _drawerSwipeState = new Subject<boolean>();

    _radRef: RadSideDrawer;

    networkChanges = this._networkSubject.asObservable();
    userChanges = this._userSubject.asObservable();
    homeChanges = this._homeSubject.asObservable();
    homeUpdation = this._homeUpdationSubject.asObservable();
    actionBarChanges = this._actionBarState.asObservable();
    actionBarTextChanges = this._actionBarText.asObservable();
    actionBarSearchChanges = this._actionBarSearch.asObservable();
    playerButtonChanges = this._playerButton.asObservable();
    drawerSwipeChanges = this._drawerSwipeState.asObservable();

    currentUser;

    constructor(private authService: AuthService) {
        this.monitorNetworkStart();

        this.networkChanges.subscribe((state: boolean) => {
            if (state) {
                this.isConnected = true;
                console.log("Connected")
            }
            else {
                this.isConnected = false;
                console.log("Not Connected")
            }
        })
    }

    newUser(user: any) {
        this._userSubject.next(user);
    }


    setUser(user: any, xRoleKey: string) {
        if (xRoleKey != undefined && xRoleKey != "" && xRoleKey != null) {
            Values.writeString(Values.X_ROLE_KEY, xRoleKey);
            this.currentUser = user;
            this._userSubject.next(this.currentUser);
        }
    }

    monitorNetworkStart() {
        // this.networkStatus = "Monitoring network connection changes.";
        connectivity.startMonitoring((newConnectionType) => {
            switch (newConnectionType) {
                case connectivity.connectionType.none:
                    this._networkSubject.next(false)
                    // this.networkStatus = "No network connection available!";
                    break;
                case connectivity.connectionType.wifi:
                    // this.networkStatus = "You are now on wifi!";
                    this._networkSubject.next(true)
                    break;
                case connectivity.connectionType.mobile:
                    // this.networkStatus = "You are now on a mobile data network!";
                    this._networkSubject.next(true)
                    break;
            }
        });
    }

    homeSelector(state: boolean) {
        this._homeSubject.next(state);
    }

    updateHomeData(data: any) {
        this._homeUpdationSubject.next(data);
    }

    openDrawer() {
        this._radRef.showDrawer();
    }

    actionBarState(state: boolean) {
        this._actionBarState.next(state);
    }

    actionBarText(text: string) {
        this._actionBarText.next(text);
    }

    buttonState(state: boolean) {
        this._playerButton.next(state);
    }

    actionBarSearch(state: boolean) {
        this._actionBarSearch.next(state);
    }

    drawerSwipe(state: boolean) {
        this._drawerSwipeState.next(state);
    }

    logout() {
        Values.writeString(Values.X_ROLE_KEY, "");
        this.currentUser = null;
        this._userSubject.next(null);
        this.authService.tnsOauthLogout();
    }

}