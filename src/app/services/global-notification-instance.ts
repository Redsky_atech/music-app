import { SongService } from "./song.service";

export class GlobalNotificationBuilder {

    public static globalNotificationBuilder: android.support.v4.app.NotificationCompat.Builder = null;
    public static snoozePendingIntent: android.app.PendingIntent = null;
    public static mainPendingIntent: android.app.PendingIntent = null;
    public static actionIntent: android.content.Intent = null;

    public static songService: SongService = null;

    public static setNotificationCompatBuilderInstance(builder: android.support.v4.app.NotificationCompat.Builder, mainPendingIntent: android.app.PendingIntent, snoozePendingIntent: android.app.PendingIntent, actionIntent: android.content.Intent) {
        this.globalNotificationBuilder = builder;
        this.snoozePendingIntent = snoozePendingIntent;
        this.mainPendingIntent = mainPendingIntent;
        this.actionIntent = actionIntent;
    }

    public static setNotificationActionIntent(actionIntent: android.content.Intent) {
        this.actionIntent = actionIntent;
    }

    public static getNotificationCompatBuilderInstance(): android.support.v4.app.NotificationCompat.Builder {
        return this.globalNotificationBuilder;
    }

    public static getMainPendingIntent(): android.app.PendingIntent {
        return this.mainPendingIntent;
    }

    public static getPendingIntent(): android.app.PendingIntent {
        return this.snoozePendingIntent;
    }

    public static getActionIntent(): android.content.Intent {
        return this.actionIntent;
    }

    public static setSongService(songService: SongService) {
        this.songService = songService;
    }

    public static getSongService() {
        return this.songService;
    }

    public static play() {
        this.songService.play();
    }

    public static pause() {
        this.songService.pause();
    }

}