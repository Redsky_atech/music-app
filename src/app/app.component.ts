import { Component, ViewChild, OnInit, AfterViewInit, OnChanges, ElementRef } from "@angular/core";
import { registerElement } from 'nativescript-angular/element-registry';
import { Carousel, CarouselItem } from 'nativescript-carousel';
import { DrawerTransitionBase, RadSideDrawer, SlideInOnTopTransition } from "nativescript-ui-sidedrawer";
import { Router } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";
import { GridLayout } from "tns-core-modules/ui/layouts/grid-layout/grid-layout";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { UserService } from "./services/user.service";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { AuthService } from "./services/auth.service";
import { Page } from "tns-core-modules/ui/page/page";
import { NotificationService } from "./services/notification.service";
import { ad } from "utils/utils"
import { SongService } from "./services/song.service";
import { resumeEvent, suspendEvent, ApplicationEventData, on as applicationOn } from "tns-core-modules/application";
import * as firebase from "nativescript-plugin-firebase";
import * as application from "tns-core-modules/application";

registerElement('Carousel', () => Carousel);
registerElement('CarouselItem', () => CarouselItem);
registerElement("PullToRefresh", () => require("nativescript-pulltorefresh").PullToRefresh);

@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "./app.component.html",
})

export class AppComponent implements OnInit, AfterViewInit, OnChanges {

    @ViewChild('rad') radDrawer: ElementRef;
    @ViewChild('gridNon') gridNonRef: ElementRef;
    @ViewChild('gridLo') gridLoRef: ElementRef;

    private androidBannerId: string = "ca-app-pub-2230394346677135/4799661034";

    gridNon: GridLayout;
    gridLo: GridLayout;

    private _rad: RadSideDrawer;
    private _page: Page;
    private _sideDrawerTransition: DrawerTransitionBase;

    homeBackgroundColor: string;
    categoriesBackgroundColor: string;
    favouritesBackgroundColor: string;
    recentMixesBackgroundColor: string;
    aboutUsBackgroundColor: string;

    homeRedLine: boolean = false;
    categoriesRedLine: boolean = false;
    favouritesRedLine: boolean = false;
    recentMixesRedLine: boolean = false;
    aboutUsRedLine: boolean = false;

    log: boolean;
    user;
    isLogged: boolean = false;

    end: boolean = false;
    name: string;
    picUrl: string = "res://inspius_logo";
    showActionBar: boolean;
    actionBarText: string
    source;

    searchState = true;
    entry;
    userService: UserService;


    constructor(private router: Router, private routerExtensions: RouterExtensions, userService: UserService, private http: HttpClient, private authService: AuthService, private page: Page, private notificationService: NotificationService, private songService: SongService) {

        this.page.actionBarHidden = true;
        this.showActionBar = false
        this.userService = userService;

        this.homeRedLine = true;
        this.categoriesRedLine = false;
        this.favouritesRedLine = false;
        this.recentMixesRedLine = false;
        this.aboutUsRedLine = false;

        this.homeBackgroundColor = "#2b343d";
        this.categoriesBackgroundColor = "transparent";
        this.favouritesBackgroundColor = "transparent";
        this.recentMixesBackgroundColor = "transparent";
        this.aboutUsBackgroundColor = "transparent";



        if (!Values.doesExist(Values.isNotNewUser)) {
            Values.writeBoolean(Values.isNotNewUser, true);

            this.user = null;
            this.isLogged = false;
            this.name = "";
            let extendedNavigationExtras: ExtendedNavigationExtras = {
                queryParams: {
                    "user": this.user
                },
                clearHistory: true
            };
            this.routerExtensions.navigate(["/detail"], extendedNavigationExtras);

        }
        else {
            if (Values.readString(Values.X_ROLE_KEY, "") != "" && Values.readString(Values.X_ROLE_KEY, "") != null && Values.readString(Values.X_ROLE_KEY, "") != undefined) {
                this.getUser(Values.readString(Values.X_ROLE_KEY, ""), function (param: any, param1: any, userServiceFun: UserService) {
                    userServiceFun.setUser(param, param1);
                });
            }
            else {
                this.user = null;
                this.isLogged = false;
                this.name = "";
                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": this.user
                    },
                    clearHistory: true,
                };
                // this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
                this.routerExtensions.navigate(["/detail"], extendedNavigationExtras);
            }
        }

        this.userService.actionBarChanges.subscribe((state: boolean) => {
            if (state != undefined) {
                this.showActionBar = state;
            }
        })

        this.userService.actionBarTextChanges.subscribe((text: string) => {
            if (text != undefined) {
                this.actionBarText = text;
            }
        })

        this.userService.actionBarSearchChanges.subscribe((state: boolean) => {
            this.searchState = state;
        })

        application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
            args.cancel = false;
            // this.routerExtensions.back();
        });

        application.on(application.suspendEvent, (args) => {
            console.log("The appication was suspended!");
            this.songService.getPlayer().pause();
            // this.notificationService.postNotification();
        });

        application.on(application.resumeEvent, (args) => {
            console.log("The appication was resumed!");
            if (this.songService.getPlayer().isAudioPlaying()) {
                this.userService.buttonState(true);
            }
            else {
                this.userService.buttonState(false);
            }
            var notificationManager = ad.getApplicationContext().getSystemService(android.content.Context.NOTIFICATION_SERVICE);
            // notificationManager.cancelAll()
        });




        applicationOn(suspendEvent, (args: ApplicationEventData) => {
            if (args.android) {
                if (this.songService.getPlayer().isAudioPlaying()) {
                    this.songService.getPlayer().pause();
                    console.log("The appication was suspended!");
                    // this.notificationService.postNotification();
                }
            }
        });

        applicationOn(resumeEvent, (args: ApplicationEventData) => {
            if (args.android) {
                if (this.songService.getPlayer().isAudioPlaying()) {

                    this.userService.buttonState(true);
                }
                else {
                    this.userService.buttonState(false);
                }
                var notificationManager = ad.getApplicationContext().getSystemService(android.content.Context.NOTIFICATION_SERVICE);
                // notificationManager.cancelAll()
            }
        });


        this.userService.userChanges.subscribe(user => {
            if (user != null) {
                this.isLogged = true;
                this.user = user;

                if (user.profile != null && user.profile != undefined) {
                    if (user.profile.firstName != null && user.profile.firstName != undefined) {
                        this.name = user.profile.firstName;
                    }
                    else {
                        this.name = user.code;
                    }

                    if (user.profile.pic != null && user.profile.pic != undefined) {
                        if (user.profile.pic.url != null && user.profile.pic.url != undefined) {
                        }
                    }
                    else {
                    }
                }
                else {
                    this.name = user.code;
                }

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": this.user
                    },
                    clearHistory: true
                };
                // this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
                this.routerExtensions.navigate(["/detail"], extendedNavigationExtras);
            }
            else {
                this.user = null;
                this.isLogged = false;
                this.name = "";

                let extendedNavigationExtras: ExtendedNavigationExtras = {
                    queryParams: {
                        "user": this.user
                    },
                    clearHistory: true
                };
                // this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
                this.routerExtensions.navigate(["/detail"], extendedNavigationExtras);
            }
        });


        this.userService.homeChanges.subscribe(state => {
            if (state) {
                this.homeRedLine = true;
                this.categoriesRedLine = false;
                this.favouritesRedLine = false;
                this.recentMixesRedLine = false;
                this.aboutUsRedLine = false;

                this.homeBackgroundColor = "#2b343d";
                this.categoriesBackgroundColor = "transparent";
                this.favouritesBackgroundColor = "transparent";
                this.recentMixesBackgroundColor = "transparent";
                this.aboutUsBackgroundColor = "transparent";
            }
            else {
                this.homeRedLine = false;
                this.categoriesRedLine = false;
                this.favouritesRedLine = false;
                this.recentMixesRedLine = false;
                this.aboutUsRedLine = false;

                this.homeBackgroundColor = "transparent";
                this.categoriesBackgroundColor = "transparent";
                this.favouritesBackgroundColor = "transparent";
                this.recentMixesBackgroundColor = "transparent";
                this.aboutUsBackgroundColor = "transparent";
            }
        });

        this.userService.drawerSwipeChanges.subscribe(state => {
            this._rad.gesturesEnabled = state
        });


        this.songService.playerStateChanges.subscribe((state: boolean) => {
            if (state) {
                // this.createBanner();
            }
            else {
                // this.hideBanner();
            }
        })

        firebase.init({
            onMessageReceivedCallback: (message) => {
                console.log(`Title: ${message.title}`);
                console.log(`Body: ${message.body}`);

                this.notificationService.postNotification(message)
            },
            onPushTokenReceivedCallback: (token: string) => {
                this.updateFirebaseToken(token);
                console.log("Firebase push token: " + token);
            }
        });


    }

    ngOnInit(): void {
        this._rad = this.radDrawer.nativeElement as RadSideDrawer;
        this.userService._radRef = this._rad;
        this._sideDrawerTransition = new SlideInOnTopTransition();
        Values.writeString(Values.SELECTED_OPTION, "home");
        this.homeRedLine = true;
        this.homeBackgroundColor = "#2b343d";
    }


    ngAfterViewInit(): void {
        this.gridNon = this.gridNonRef.nativeElement as GridLayout;
        this.gridLo = this.gridLoRef.nativeElement as GridLayout;
    }

    public updateFirebaseToken(token: string) {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.http.put("http://send-it-api-dev.m-sas.com/api/users/my", { "devices": [{ "id": token, "status": "active" }] }, { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data
                this.user = result;
            }
            else {
            }
        },
            error => {
            })

    }

    public createBanner() {
        firebase.admob.showBanner({
            size: firebase.admob.AD_SIZE.SMART_BANNER, // see firebase.admob.AD_SIZE for all options
            margins: { // optional nr of device independent pixels from the top or bottom (don't set both)
                bottom: 0,
                top: 8
            },
            androidBannerId: "ca-app-pub-4002950143768902/8732586874",
            testing: true, // when not running in production set this to true, Google doesn't like it any other way
            keywords: ["keyword1", "keyword2"] // add keywords for ad targeting
        }).then(
            function () {
                console.log("AdMob banner showing");
            },
            function (errorMessage) {
                console.log("Error" + errorMessage);
            }
        );
    }

    public hideBanner() {
        firebase.admob.hideBanner().then(
            function () {
                console.log("AdMob banner hidden");
            },
            function (errorMessage) {
                console.log(errorMessage);
            }
        );
    }


    showInterstitial() {
        firebase.admob.preloadInterstitial({
            androidInterstitialId: "ca-app-pub-4002950143768902/1209320072",
            testing: true, // when not running in production set this to true, Google doesn't like it any other way
            onAdClosed: () => console.log("Interstitial closed")
        }).then(
            function () {
                firebase.admob.showInterstitial();
            },
            function (errorMessage) {
                console.log(errorMessage)
            }
        );
    }

    onOpenDrawer(args) {
        console.log(args);
    }

    ngOnChanges() {
    }

    getsideDrawerTransition(): DrawerTransitionBase {
        return this._sideDrawerTransition;
    }

    getUser(xRoleKey: string, callback) {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-tenant-code": "music",
            "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
        });

        this.http.get("http://ems-api-dev.m-sas.com/api/users/my", { headers: headers }).subscribe((res: any) => {

            if (res.isSuccess) {
                let result: any
                result = res.data
                callback(result, xRoleKey, this.userService);
                this.user = result;
            }
            else {
                console.log(res.error)
                return null;
            }
        },
            error => {
                console.log(error)
                return null;
            })
    }

    onDrawerButtonTap() {
        this._rad.showDrawer();
    }

    onSearchButtonTap() {
        this.routerExtensions.navigate(["/search"]);
    }

    onSignIn() {
        this._rad.closeDrawer();
        this.routerExtensions.navigate(["/login"]);
    }

    onRegister() {

        this._rad.closeDrawer();

        this.routerExtensions.navigate(["/register"]);

    }

    onMyAccount() {

        this._rad.closeDrawer();

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "user": this.user
            }
        };
        this.userService.homeSelector(false)
        this.routerExtensions.navigate(["/myAccount"], extendedNavigationExtras);
    }

    onDisplayPic() {

    }

    onLogout() {
        this._rad.closeDrawer();

        this.userService.logout();
        this.isLogged = false;
        // this.routerExtensions.navigate(["/home"]);
        this.routerExtensions.navigate(["/detail"])
    }

    onHomeClicked() {
        this.userService.actionBarState(true);
        this.userService.actionBarText("HOME");
        this._rad.closeDrawer();

        this.homeRedLine = true;
        this.categoriesRedLine = false;
        this.favouritesRedLine = false;
        this.recentMixesRedLine = false;
        this.aboutUsRedLine = false;

        this.homeBackgroundColor = "#2b343d";
        this.categoriesBackgroundColor = "transparent";
        this.favouritesBackgroundColor = "transparent";
        this.recentMixesBackgroundColor = "transparent";
        this.aboutUsBackgroundColor = "transparent";

        let extendedNavigationExtras: ExtendedNavigationExtras = {
            queryParams: {
                "user": this.user
            },
            clearHistory: true
        };

        // this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
        this.routerExtensions.navigate(["/detail"], extendedNavigationExtras);

    }

    // onCategoriesClicked() {

    //     this._rad.closeDrawer();

    //     this.homeRedLine = false;
    //     this.categoriesRedLine = true;
    //     this.favouritesRedLine = false;
    //     this.recentMixesRedLine = false;
    //     this.aboutUsRedLine = false;

    //     this.homeBackgroundColor = "transparent";
    //     this.categoriesBackgroundColor = "#2b343d";
    //     this.favouritesBackgroundColor = "transparent";
    //     this.recentMixesBackgroundColor = "transparent";
    //     this.aboutUsBackgroundColor = "transparent";
    //     let extendedNavigationExtras: ExtendedNavigationExtras = {
    //         queryParams: {
    //             "user": this.user
    //         },
    //         clearHistory: true
    //     };
    //     this.routerExtensions.navigate(["/categories"], extendedNavigationExtras);
    // }

    // onFavouritesClicked() {

    //     this._rad.closeDrawer();

    //     this.homeRedLine = false;
    //     this.categoriesRedLine = false;
    //     this.favouritesRedLine = true;
    //     this.recentMixesRedLine = false;
    //     this.aboutUsRedLine = false;

    //     this.homeBackgroundColor = "transparent";
    //     this.categoriesBackgroundColor = "transparent";
    //     this.favouritesBackgroundColor = "#2b343d";
    //     this.recentMixesBackgroundColor = "transparent";
    //     this.aboutUsBackgroundColor = "transparent";
    //     let extendedNavigationExtras: ExtendedNavigationExtras = {
    //         queryParams: {
    //             "user": this.user
    //         },
    //         clearHistory: true
    //     };
    //     this.routerExtensions.navigate(["/favourites"], extendedNavigationExtras);
    // }

    // onRecentMixesClicked() {

    //     this._rad.closeDrawer();

    //     this.homeRedLine = false;
    //     this.categoriesRedLine = false;
    //     this.favouritesRedLine = false;
    //     this.recentMixesRedLine = true;
    //     this.aboutUsRedLine = false;

    //     this.homeBackgroundColor = "transparent";
    //     this.categoriesBackgroundColor = "transparent";
    //     this.favouritesBackgroundColor = "transparent";
    //     this.recentMixesBackgroundColor = "#2b343d";
    //     this.aboutUsBackgroundColor = "transparent";
    //     let extendedNavigationExtras: ExtendedNavigationExtras = {
    //         queryParams: {
    //             "user": this.user
    //         },
    //         clearHistory: true
    //     };
    //     this.routerExtensions.navigate(["/recentMixes"], extendedNavigationExtras);
    // }

    onAboutUsClicked() {
        this.userService.actionBarState(true);
        this.userService.actionBarText("About Us");

        this.homeRedLine = false;
        this.categoriesRedLine = false;
        this.favouritesRedLine = false;
        this.recentMixesRedLine = false;
        this.aboutUsRedLine = true;

        this.homeBackgroundColor = "transparent";
        this.categoriesBackgroundColor = "transparent";
        this.favouritesBackgroundColor = "transparent";
        this.recentMixesBackgroundColor = "transparent";
        this.aboutUsBackgroundColor = "#2b343d";
        this._rad.closeDrawer();

        this.routerExtensions.navigate(["/aboutUs"]);
    }

}

