import { NgModule } from "@angular/core";
import { Routes, PreloadAllModules } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { PlayerComponent } from "./player/components/player.component";

const routes: Routes = [

   { path: "player", component: PlayerComponent },
   { path: "home", loadChildren: "~/app/home/home.module#HomeModule" },
   { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
   { path: "register", loadChildren: "~/app/register/register.module#RegisterModule" },
   { path: "intro", loadChildren: "~/app/intro/intro.module#IntroModule" },
   { path: "detail", loadChildren: "~/app/detail/detail.module#DetailModule" },
   // { path: "myAccount", loadChildren: "~/app/myaccount/myaccount.module#MyAccountModule" },
   // { path: "accountInfo", loadChildren: "~/app/accountinfo/accountinfo.module#AccountInfoModule" },
   // { path: "changePassword", loadChildren: "~/app/changepassword/changepassword.module#ChangePasswordModule" },
   // { path: "favourites", loadChildren: "~/app/favourites/favourites.module#FavouritesModule" },
   // { path: "recentMixes", loadChildren: "~/app/recentmixes/recentmixes.module#RecentMixesModule" },
   { path: "aboutUs", loadChildren: "~/app/aboutus/aboutus.module#AboutUsModule" },
   // { path: "categories", loadChildren: "~/app/categories/categories.module#CategoriesModule" },
   // { path: "categoryFiles", loadChildren: "~/app/category-files/category-files.module#CategoryFilesModule" },
   // { path: "comments", loadChildren: "~/app/comments/comments.module#CommentsModule" },
   // { path: "search", loadChildren: "~/app/search/search.module#SearchModule" },
];

@NgModule({
   imports: [NativeScriptRouterModule.forRoot(
      <any>routes, { preloadingStrategy: PreloadAllModules })],
   exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }