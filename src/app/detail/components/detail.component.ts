import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute, NavigationExtras, Router } from "@angular/router";
import { UserService } from "~/app/services/user.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Observable, EventData } from "tns-core-modules/data/observable/observable";
import { Page, isAndroid } from "tns-core-modules/ui/page/page";
import { Values } from "~/app/values/values";
import { fromBase64, ImageSource, fromResource } from "tns-core-modules/image-source/image-source";
import { Comment } from "~/app/models/comment";
import * as Toast from 'nativescript-toast'
import * as application from "tns-core-modules/application";
import { SongService } from "~/app/services/song.service";


class Song {
    constructor(public name: string) { }
}

// songs = ["https://jatt.download/music/data/Hindi_Movies/201802/Sonu_Ke_Titu_Ki_Sweety/128/Bom_Diggy_Diggy.mp3", "https://jatt.download/music/data/Hindi_Movies/201902/Luka_Chuppi/128/Coca_Cola.mp3", "https://jatt.download/music/data/Hindi_Movies/201808/Satyameva_Jayate/128/Dilbar.mp3"]

let songNames = ["https://jatt.download/music/data/Hindi_Movies/201802/Sonu_Ke_Titu_Ki_Sweety/128/Bom_Diggy_Diggy.mp3", "https://jatt.download/music/data/Hindi_Movies/201902/Luka_Chuppi/128/Coca_Cola.mp3", "https://jatt.download/music/data/Hindi_Movies/201808/Satyameva_Jayate/128/Dilbar.mp3", "https://cdn10.upload.solutions/507337cbad4d644b32c16a9b4362b5b5/xiqov/Kya Baat Ay-(Mr-Jatt.com).mp3", "https://cdn10.upload.solutions/647f71bfd6c20eb51b20084f134727be/tbaov/Viah-(Mr-Jatt.com).mp3", "Czech Republic",
    "https://cdn7.upload.solutions/afd29c2f81fb508bab015a68318bb654/xiuuv/Dil Luteya-(Mr-Jatt.com).mp3", "https://cdn7.upload.solutions/edb1d4a06d73658bc09b31a6e962c446/vxhuv/Affair-(Mr-Jatt.com).mp3", "https://cdn5.upload.solutions/f6ed863a2cae87daeeb1a5a81efa5188/mqmnv/Ku Ku-(Mr-Jatt.com).mp3", "https://cdn10.upload.solutions/5a9cb493a1df274a352a06d059ea6536/qxqov/Devil-(Mr-Jatt.com).mp3", "Germany", "Greece", "Hungary", "Ireland", "Italy",
    "Latvia", "Lithuania", "Luxembourg", "Malta", "Netherlands", "Poland", "Portugal", "Romania", "Slovakia",
    "Slovenia", "Spain", "Sweden", "United Kingdom"];

var songs = [{
    "title": "Bom Diggy Diggy",
    "url": "https://jatt.download/music/data/Hindi_Movies/201802/Sonu_Ke_Titu_Ki_Sweety/128/Bom_Diggy_Diggy.mp3"
},
{
    "title": "Coca Cola",
    "url": "https://jatt.download/music/data/Hindi_Movies/201902/Luka_Chuppi/128/Coca_Cola.mp3"
},
{
    "title": "Dilbar",
    "url": "https://jatt.download/music/data/Hindi_Movies/201808/Satyameva_Jayate/128/Dilbar.mp3"
},
{
    "title": "Kya Batt Ay",
    "url": "https://cdn10.upload.solutions/507337cbad4d644b32c16a9b4362b5b5/xiqov/Kya Baat Ay-(Mr-Jatt.com).mp3"
},
{
    "title": "Viah",
    "url": "https://cdn10.upload.solutions/647f71bfd6c20eb51b20084f134727be/tbaov/Viah-(Mr-Jatt.com).mp3"
},
{
    "title": "Dil Luteya",
    "url": "https://cdn7.upload.solutions/afd29c2f81fb508bab015a68318bb654/xiuuv/Dil Luteya-(Mr-Jatt.com).mp3"
},
{
    "title": "Affair",
    "url": "https://cdn7.upload.solutions/edb1d4a06d73658bc09b31a6e962c446/vxhuv/Affair-(Mr-Jatt.com).mp3"
},
{
    "title": "Ku Ku",
    "url": "https://cdn5.upload.solutions/f6ed863a2cae87daeeb1a5a81efa5188/mqmnv/Ku Ku-(Mr-Jatt.com).mp3"
},
{
    "title": "Devil",
    "url": "https://cdn10.upload.solutions/5a9cb493a1df274a352a06d059ea6536/qxqov/Devil-(Mr-Jatt.com).mp3"
}]

@Component({
    selector: "Detail",
    moduleId: module.id,
    templateUrl: "./detail.component.html",
    styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

    songs = [{
        "title": "Bom Diggy Diggy",
        "url": "https://jatt.download/music/data/Hindi_Movies/201802/Sonu_Ke_Titu_Ki_Sweety/128/Bom_Diggy_Diggy.mp3"
    },
    {
        "title": "Coca Cola",
        "url": "https://jatt.download/music/data/Hindi_Movies/201902/Luka_Chuppi/128/Coca_Cola.mp3"
    },
    {
        "title": "Dilbar",
        "url": "https://jatt.download/music/data/Hindi_Movies/201808/Satyameva_Jayate/128/Dilbar.mp3"
    },
    {
        "title": "Kya Batt Ay",
        "url": "https://cdn10.upload.solutions/507337cbad4d644b32c16a9b4362b5b5/xiqov/Kya Baat Ay-(Mr-Jatt.com).mp3"
    },
    {
        "title": "Viah",
        "url": "https://cdn10.upload.solutions/647f71bfd6c20eb51b20084f134727be/tbaov/Viah-(Mr-Jatt.com).mp3"
    },
    {
        "title": "Dil Luteya",
        "url": "https://cdn7.upload.solutions/afd29c2f81fb508bab015a68318bb654/xiuuv/Dil Luteya-(Mr-Jatt.com).mp3"
    },
    {
        "title": "Affair",
        "url": "https://cdn7.upload.solutions/edb1d4a06d73658bc09b31a6e962c446/vxhuv/Affair-(Mr-Jatt.com).mp3"
    },
    {
        "title": "Ku Ku",
        "url": "https://cdn5.upload.solutions/f6ed863a2cae87daeeb1a5a81efa5188/mqmnv/Ku Ku-(Mr-Jatt.com).mp3"
    },
    {
        "title": "Devil",
        "url": "https://cdn10.upload.solutions/5a9cb493a1df274a352a06d059ea6536/qxqov/Devil-(Mr-Jatt.com).mp3"
    }]

    // page;
    // songName;
    // songId;
    // songThumbnail;
    // songUrl;
    // songViews;
    // songIsFavourite;
    // thumbnail;

    // favouriteClass: string;
    listBackground: string;

    commentsExist: boolean = false;
    comments = new ObservableArray();
    // viewModel;
    noComments: boolean;
    // isRendering: boolean;
    // renderViewTimeout;
    // isLoadingComments: boolean;
    // isFavouring: boolean;
    constructor(private songService: SongService, private activatedRoute: ActivatedRoute, private router: Router, private routerExtensions: RouterExtensions, private userService: UserService, private http: HttpClient) {

        this.userService.homeSelector(false);
        // this.favouriteClass = "favicon"
        this.noComments = true;
        this.listBackground = "Unselected";
        // this.isRendering = false;
        // this.isLoadingComments = false;
        // this.isFavouring = false;
        // this.activatedRoute.queryParams.subscribe(params => {
        // this.songName = params.name;
        // this.songId = params.id;
        // this.songThumbnail = params.thumbnail;
        // this.songUrl = params.url;
        // this.songViews = params.views;

        // this.songIsFavourite = <boolean>params.isFavourite;

        //     if (this.songIsFavourite) {
        //         this.favouriteClass = "favicon-clicked"
        //     }
        //     if (this.songName != null && this.songName != undefined && this.songName != "") {
        //         this.userService.actionBarState(true);
        //         this.userService.actionBarText("Details")

        //     }
        //     if (this.songId != null && this.songId != undefined && this.songId != "") {

        //     }

        //     if (this.songThumbnail != null && this.songThumbnail != undefined) {
        //         if (<ImageSource>this.getThumbnailSrc(this.songThumbnail) != null && <ImageSource>this.getThumbnailSrc(this.songThumbnail) != undefined) {
        //             this.thumbnail = <ImageSource>this.getThumbnailSrc(this.songThumbnail);

        //         }
        //         else {
        //             this.thumbnail = <ImageSource>fromResource("img_video_default");
        //         }
        //     }
        //     else {
        //         this.thumbnail = <ImageSource>fromResource("img_video_default");
        //     }
        // })

        // this.userService.userChanges.subscribe(user => {
        //     if (user == null || user == undefined) {

        //         let extendedNavigationExtras: ExtendedNavigationExtras = {
        //             queryParams: {
        //                 "user": null
        //             },
        //         };
        //         this.routerExtensions.navigate(["/home"], extendedNavigationExtras)
        //     }
        // })


    }

    public onSongTap(args) {

        this.listBackground = "Selected"

        let navigationExtras: NavigationExtras = {
            queryParams: {
                // "id": this.songId,
                // "name": this.songName,
                // "thumbnail": this.songThumbnail,
                "song": JSON.stringify(this.songs[args.index])
                // "isFavourite": this.songIsFavourite
            },
        };
        console.log("SOSOSOS", this.songs[args.index])
        this.router.navigate(["/player"], navigationExtras)
    }

    ngOnInit(): void {
        if (isAndroid) {
            application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
                args.cancel = true;
                // this.routerExtensions.back();
            });
        }
        console.log("SHOOOOOOOO", this.songService)
    }

    pageLoaded(args: EventData) {
        // this.page = args.object as Page;
        // this.renderViewTimeout = setTimeout(() => {
        //     this.isRendering = true;
        // }, 500)
        // this.getComments();

        // if (this.comments == undefined || this.comments == null || this.comments.length == 0) {
        //     this.commentsExist = false;
        // }
        // else {
        //     this.commentsExist = true;
        // }
        console.log("Page Loaded called")
    }


    pageUnloaded() {
        // this.comments = new ObservableArray();
    }

    // getThumbnailSrc(data: string) {
    //     if (data != null && data != undefined) {
    //         let base64Data = data.split(',');
    //         if (base64Data.length == 2) {
    //             if (base64Data[1] != null && base64Data[1] != undefined) {
    //                 const imageSrc = fromBase64(base64Data[1]);
    //                 return imageSrc;
    //             }
    //             else {
    //                 const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
    //                 return imgFromResources;
    //             }
    //         } else {
    //             const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
    //             return imgFromResources;
    //         }
    //     }
    //     else {
    //         const imgFromResources: ImageSource = <ImageSource>fromResource("img_video_default");
    //         return imgFromResources;
    //     }
    // }

    // setFavouriteSong(songId: string, state: boolean) {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-tenant-code": "music",
    //         "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
    //     });
    //     this.isFavouring = true;

    //     var body = { "isFavourite": state }

    //     this.http.put("http://docs-api-dev.m-sas.com/api/files/" + songId, body, { headers: headers }).subscribe((res: any) => {

    //         if (res.isSuccess) {

    //             if (<boolean>res.data.isFavourite) {
    //                 Toast.makeText("Added to Favourites").show();
    //             }
    //             else {
    //                 Toast.makeText("Removed from Favourites").show();

    //             }

    //             this.songIsFavourite = <boolean>res.data.isFavourite;

    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isFavouring = false;
    //             }, 500)
    //         }
    //         else {
    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isFavouring = false;
    //             }, 500)
    //         }
    //     },
    //         error => {
    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isFavouring = false;
    //             }, 500)
    //         })
    // }

    // getComments() {
    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "x-tenant-code": "music",
    //         "x-role-key": Values.readString(Values.X_ROLE_KEY, "")
    //     });
    //     this.isLoadingComments = true;

    //     this.http.get("http://rating-api-dev.m-sas.com/api/files/" + this.songId + "/ratingLogs", { headers: headers }).subscribe((res: any) => {

    //         if (res.isSuccess) {
    //             let result: any
    //             result = res.items
    //             for (var i = 0; i < result.length; i++) {
    //                 this.comments.push(new Comment(result[i]))
    //             }

    //             if (this.comments.length != 0) {
    //                 this.noComments = false;
    //             }

    //             this.viewModel = new Observable();
    //             this.viewModel.set("items", this.comments);

    //             this.page.bindingContext = this.viewModel;
    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isLoadingComments = false;
    //             }, 500)
    //         }
    //         else {
    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isLoadingComments = false;
    //             }, 500)
    //         }
    //     },
    //         error => {
    //             this.renderViewTimeout = setTimeout(() => {
    //                 this.isLoadingComments = false;
    //             }, 500)
    //         })
    // }

    // onTitleClick() {

    //     let extendedNavigationExtras: ExtendedNavigationExtras = {
    //         queryParams: {
    //             "id": this.songId,
    //             "name": this.songName,
    //             "thumbnail": this.songThumbnail,
    //             "url": this.songUrl,
    //             "isFavourite": this.songIsFavourite
    //         },
    //     };
    //     this.routerExtensions.navigate(["/player"], extendedNavigationExtras)
    // }

    // onFavouriteClick() {

    //     if (this.favouriteClass == "favicon-clicked") {
    //         this.favouriteClass = "favicon"
    //         this.setFavouriteSong(this.songId, false)
    //     }
    //     else {
    //         this.favouriteClass = "favicon-clicked"
    //         this.setFavouriteSong(this.songId, true)
    //     }

    // }

    // onCommentClick() {
    // let extendedNavigationExtras: ExtendedNavigationExtras = {
    //     queryParams: {
    //         "id": this.songId,
    //         "name": this.songName,
    //         "thumbnail": this.songThumbnail,
    //         "url": this.songUrl,
    //         "isFavourite": this.songIsFavourite,
    //         "views": this.songViews
    //     },
    // };
    // this.routerExtensions.navigate(["/comments"], extendedNavigationExtras)
    // }

    ngOnDestroy(): void {
        if (isAndroid) {
            application.off(application.AndroidApplication.activityBackPressedEvent, (result) => {
                console.log("Removed")
            });

        }
        // clearTimeout(this.renderViewTimeout);
    }

}