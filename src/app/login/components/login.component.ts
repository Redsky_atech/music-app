import { Component, ElementRef, ViewChild } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router"
import { alert } from "tns-core-modules/ui/dialogs/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { User } from "~/app/models/user";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { UserService } from "~/app/services/user.service";
import { ModalComponent } from "~/app/modal/modal.component";
import { ITnsOAuthTokenResult } from "nativescript-oauth2";

import { Page, EventData } from "tns-core-modules/ui/page/page";
import { topmost } from "tns-core-modules/ui/frame";
import { AuthService } from "~/app/services/auth.service";
import { Pic } from "~/app/models/pic";
import { Profile } from "~/app/models/profile";
import { getJSON } from "tns-core-modules/http";


@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css'],
})

export class LoginComponent {
    @ViewChild('otpDialog') otpModal: ModalComponent;
    @ViewChild('passwordDialog') passwordModal: ModalComponent;
    @ViewChild('activityPassword') activityPasswordRef: ElementRef;
    @ViewChild('activityLogin') activityLoginRef: ElementRef;
    @ViewChild('activityForgotPassword') activityForgotPasswordRef: ElementRef;

    page;

    transitions = ["explode", "fade", "flip", "flipLeft", "slide", "slideRight", "slideTop", "slideBottom"];
    btnEmployeeStyle = 'btn-employee';
    btnStudentStyle = 'btn-student';
    inputStyle = 'inputInactive';
    btnSelection = null;
    http: HttpClient;
    textfieldData: string;
    textfield: TextField;
    url: string;
    labelClass;
    forgot_password: boolean;
    hide: boolean;
    emailTerm: string;
    emailTerm2: string;

    userNameText: string = "";
    passwordText: string = "";
    picUrl: string = "";
    emailText: string = "";
    otpText: string = "";
    newPasswordText: string = "";
    confirmNewPasswordText: string = "";

    userNameIcon: string;
    passwordIcon: string;
    emailIcon: string;

    user: User;
    userId: string;
    loggedIn: boolean;
    res: any;

    underlineUserName: string;
    underlinePassword: string;
    underlineEmail: string;
    FACEBOOK_GRAPH_API_URL: "https://graph.facebook.com/v2.9"

    @ViewChild("password") password: ElementRef;
    @ViewChild("confirmPassword") confirmPassword: ElementRef;

    result: string;
    isBusy: boolean = false;
    isSubmittingEmail: boolean;
    isLoginingIn: boolean;
    isRendering: boolean;
    isSubmittingPassword: boolean;
    renderViewTimeout;
    constructor(private pageView: Page, private routerExtensions: RouterExtensions, http: HttpClient, private userService: UserService, private authService: AuthService) {
        this.http = http;
        this.pageView.actionBarHidden = true;
        this.user = new User();
        this.loggedIn = false;
        this.userService.homeSelector(false);
        this.isRendering = false;
        this.userService.actionBarState(false)
        this.isLoginingIn = false;
        this.isSubmittingEmail = false;
        this.emailTerm = "Please enter the email you used to register. Check"
        this.emailTerm2 = "your email for the instructions to receive your password"

        this.userNameIcon = "res://icon_username"
        this.passwordIcon = "res://icon_password"
        this.emailIcon = "res://icon_email"

        this.underlineUserName = "#ffffff"
        this.underlinePassword = "#ffffff"
        this.underlineEmail = "#ffffff"
    }

    ngOnInit(): void {
        this.inputStyle = 'inputInactive';
        this.labelClass = "labels"
        this.forgot_password = false;
        this.renderViewTimeout = setTimeout(() => {
            this.isRendering = true;
        }, 1000)
    }

    public onTapLogin() {
        // const fram = topmost();

        // this.authService
        //     .tnsOauthLogin("facebook")
        //     .then((result: ITnsOAuthTokenResult) => {
        //         console.log("back to app component with token" + result.accessToken);
        //         Values.writeString(Values.FACEBOOK_ACCESS_TOKEN, result.accessToken)
        //         getJSON("https://graph.facebook.com/v2.9" + "/me?access_token=" + result.accessToken).then((res: any) => {
        //             this.userNameText = res.name;
        //             this.userId = res.id;
        //             this.user = new User();
        //             this.user.facebookId = this.userId;

        //             getJSON("https://graph.facebook.com/v2.9" + "/" + this.userId + "/picture?type=large&redirect=false&access_token=" + result.accessToken).then((res: any) => {
        //                 this.picUrl = res.data.url;
        //                 var name = this.userNameText.split(" ");

        //                 var lastName = '';
        //                 var firstName = '';
        //                 if (name != undefined && name != null) {
        //                     var len = name.length;
        //                     if (len - 1 != -1) {
        //                         if (len - 1 == 0) {
        //                             firstName = name[len - 1]
        //                         }
        //                         else {
        //                             for (var i = 0; i < len - 1; i++) {
        //                                 firstName = firstName + name[i];
        //                             }
        //                             lastName = name[len - 1];
        //                         }
        //                     }
        //                 }

        //                 var pic = new Pic();
        //                 pic.url = this.picUrl;
        //                 var profile = new Profile();
        //                 profile.firstName = firstName;
        //                 profile.lastName = lastName;
        //                 profile.pic = pic;

        //                 this.user.profile = profile;

        //                 let headers = new HttpHeaders({
        //                     "Content-Type": "application/json",
        //                     "x-tenant-code": "music"
        //                 });

        //                 this.http.post("http://ems-api-dev.m-sas.com/api/users/signUp", this.user, { headers: headers }).subscribe((response: any) => {

        //                     if (response.isSuccess) {

        //                         let result: any
        //                         result = response.data


        //                         for (var i = 0; i < result.roles.length; i++) {
        //                             if (result.roles[i] != undefined && result.roles[i].key != undefined && result.roles[i].key != "") {
        //                                 if (result.roles[i].isDefaultRole) {
        //                                     Values.writeString(Values.X_ROLE_KEY, result.roles[i].key);
        //                                     let extendedNavigationExtras: ExtendedNavigationExtras = {
        //                                         queryParams: {
        //                                             "user": result
        //                                         }
        //                                     };
        //                                     this.userService.setUser(result, result.roles[i].key);
        //                                     this.routerExtensions.navigate(["/app"], extendedNavigationExtras);
        //                                 }
        //                             }
        //                             else {
        //                                 alert("Authentication Problem (Could not get role key)");
        //                             }
        //                         }

        //                     }
        //                     else {
        //                         alert(response.error);
        //                     }

        //                 }, error => {
        //                     alert(error);
        //                 });

        //             }, function (err) {
        //                 alert("Error getting user info: " + err);
        //             });
        //         }, function (err) {
        //             alert("Error getting user info: " + err);
        //         });



        //     }).then(() => {
        //         this.routerExtensions.navigate(["/home"])
        //     });
    }

    public onTapLogout() {
        this.authService.tnsOauthLogout();
    }

    pageLoaded(args: EventData) {
        this.page = args.object as Page;
    }


    pageUnloaded() {
    }

    onFocusUserName() {
        this.userNameIcon = "res://icon_username_hover"
        this.underlineUserName = "#cc3333"
    }

    onBlurUserName() {
        this.userNameIcon = "res://icon_username"
        this.underlineUserName = "#ffffff"
    }

    public userNameTextField(args) {
        var textField = <TextField>args.object;
        this.userNameText = textField.text;
    }

    onFocusPassword() {
        this.passwordIcon = "res://icon_password_hover"
        this.underlinePassword = "#cc3333"
    }

    onBlurPassword() {
        this.passwordIcon = "res://icon_password"
        this.underlinePassword = "#ffffff"
    }

    public passwordTextField(args) {
        var textField = <TextField>args.object;
        this.passwordText = textField.text;
    }

    onFocusEmail() {
        this.emailIcon = "res://icon_email_hover"
        this.underlineEmail = "#cc3333"
    }

    onBlurEmail() {
        this.emailIcon = "res://icon_email"
        this.underlineEmail = "#ffffff"
    }

    public emailTextField(args) {
        var textField = <TextField>args.object;
        this.emailText = textField.text;
    }


    public otpTextField(args) {
        var textField = <TextField>args.object;
        this.otpText = textField.text;
    }

    public newPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.newPasswordText = textField.text;
    }

    public confirmNewPasswordTextField(args) {
        var textField = <TextField>args.object;
        this.confirmNewPasswordText = textField.text;
    }


    onLoginClick() {

        // if (this.userNameText == "") {
        //     alert("Username can not be empty");
        //     return;
        // }

        // if (this.passwordText == "") {
        //     alert("Passsword can not be empty");
        //     return;
        // }

        // let headers = new HttpHeaders({
        //     "Content-Type": "application/json",
        //     "x-tenant-code": "music"
        // });

        // this.user.code = this.userNameText;
        // this.user.password = this.passwordText;
        // this.isLoginingIn = true;
        // this.http.post("http://ems-api-dev.m-sas.com/api/users/signIn", this.user, { headers: headers }).subscribe((res: any) => {

        //     if (res.isSuccess) {

        //         let result: any
        //         result = res.data
        //         this.res = result;
        //         for (var i = 0; i < result.roles.length; i++) {
        //             if (result.roles[i] != undefined && result.roles[i].key != undefined && result.roles[i].key != "") {
        //                 if (result.roles[i].isDefaultRole) {
        //                     Values.writeString(Values.X_ROLE_KEY, result.roles[i].key);
        //                     let extendedNavigationExtras: ExtendedNavigationExtras = {
        //                         queryParams: {
        //                             "user": result
        //                         }
        //                     };
        //                     this.userService.setUser(result, result.roles[i].key);
        //                     this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
        //                 }

        //             }
        //             else {
        //                 alert("Authentication Problem (Could not get role key)");
        //             }
        //         }

        //         setTimeout(() => {
        //             this.isLoginingIn = false;
        //         }, 500)
        //     }
        //     else {
        //         setTimeout(() => {
        //             this.isLoginingIn = false;
        //         }, 5000)
        //     }
        // },
        //     error => {
        //         setTimeout(() => {
        //             this.isLoginingIn = false;
        //         }, 5000)
        //     })
    }

    forgotPasswordSubmit() {

        // this.hide = false;

        // if (this.emailText == "") {
        //     alert("Email can not be empty");
        //     return;
        // }

        // let headers = new HttpHeaders({
        //     "Content-Type": "application/json",
        //     "x-tenant-code": "music"
        // });
        // this.user.email = this.emailText;
        // this.isSubmittingEmail = true;


        // this.http.post("http://ems-api-dev.m-sas.com/api/users/resend", this.user, { headers: headers }).subscribe((res: any) => {

        //     if (res.isSuccess) {
        //         this.res = res;
        //         setTimeout(() => {
        //             this.isSubmittingEmail = false;
        //         }, 500)
        //         this.openOtpDialog();
        //     }
        //     else {
        //         setTimeout(() => {
        //             this.isSubmittingEmail = false;
        //         }, 500)
        //         alert(res.error);
        //     }

        // },
        //     error => {
        //         setTimeout(() => {
        //             this.isSubmittingEmail = false;
        //         }, 500)
        //         alert(error);
        //     })
    }



    onOtpSubmit() {
        if (this.otpText.length != 6) {
            alert("Please enter 6 digit OTP")
            return;
        }
        this.res.data.otp = this.otpText;
        this.otpModal.hide();
        this.openPasswordDialog();
    }

    onPasswordSubmit() {

        // if (this.newPasswordText != this.confirmNewPasswordText) {
        //     alert("Passwords do not match")
        //     return;
        // }
        // let headers = new HttpHeaders({
        //     "Content-Type": "application/json",
        //     "x-tenant-code": "music"
        // });
        // this.isSubmittingPassword = true;
        // this.res.data.password = this.newPasswordText;

        // this.http.post("http://ems-api-dev.m-sas.com/api/users/setPassword/" + this.res.data.id, this.res.data, { headers: headers }).subscribe((res: any) => {
        //     let result;
        //     if (res.isSuccess) {
        //         result = res.data;
        //         this.res = res;
        //         this.isBusy = false;
        //         this.passwordModal.hide();
        //         for (var i = 0; i < result.roles.length; i++) {
        //             if (result.roles[i] != undefined && result.roles[i].key != undefined && result.roles[i].key != "") {
        //                 Values.writeString(Values.X_ROLE_KEY, result.roles[i].key);
        //                 let extendedNavigationExtras: ExtendedNavigationExtras = {
        //                     queryParams: {
        //                         "user": result
        //                     }
        //                 };
        //                 this.userService.setUser(result, result.roles[i].key);
        //                 this.userService.homeSelector(true);
        //                 this.routerExtensions.navigate(["/home"], extendedNavigationExtras);
        //             }
        //             else {
        //                 setTimeout(() => {
        //                     this.isSubmittingPassword = false;
        //                 }, 500)
        //                 alert("Authentication Problem (Could not get role key)");
        //             }
        //         }
        //     }
        //     else {
        //         this.isBusy = false;
        //         setTimeout(() => {
        //             this.isSubmittingPassword = false;
        //         }, 500)
        //     }

        // },
        //     error => {
        //         setTimeout(() => {
        //             this.isSubmittingPassword = false;
        //         }, 500)
        //         this.isBusy = false;
        //         alert(error);
        //     })

    }

    nativegateBack(): void {
        this.routerExtensions.back();
    }

    navigateToRegister() {
        this.routerExtensions.navigate(["/register"]);
    }


    nativateToForgotPassword() {
        this.forgot_password = true;
        this.hide = true;
    }

    navigateFromForgotPassword() {
        this.forgot_password = false;
    }


    onTap() {
        alert("clicked an item");
    }


    openOtpDialog() {
        this.otpModal.show();
    }

    closeOtpDialog() {
        this.otpModal.hide();
    }

    openPasswordDialog() {
        this.passwordModal.show();
    }

    closePasswordDialog() {
        this.passwordModal.hide();
    }


    onOpenModal() {
        console.log("opened modal");
    }

    onCloseModal() {
        console.log("closed modal");
    }

    ngOnDestroy(): void {
        clearTimeout(this.renderViewTimeout);

    }
}